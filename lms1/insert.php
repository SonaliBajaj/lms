
<?php
include("connection.php");

if(isset($_POST['submit']))
{
    $name = $_POST['name'];
    $date = $_POST['date'];
  @  $gen  = $_POST['gender'];
    $mno = $_POST['mno'];
    $desc = $_POST['desc'];
 
     $q="insert into authors (name,dob,gender,mobno,desci) values('$name','$date','$gen','$mno','$desc')" or die($mysqli->error);
    echo $ans=mysqli_query($conn,$q);
        header("location:display.php");
    
}
 
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
  
    <title>FORM</title>
    <style>
   
    </style>
</head>
<?php
$nameErr = $genderErr = $mnoErr = $descErr ="";
$name = $desc = $gender = $mno ="";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["name"])) {
      $nameErr = "Name is required";
    } else {
      $name = test_input($_POST["name"]);
      // check if name only contains letters and whitespace
      if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
        $nameErr = "Only letters and white space allowed";
      }
    }
    if (empty($_POST["gender"])) {
        $genderErr = "Gender is required";
      } else {
        $gender = test_input($_POST["gender"]);
      }

      if (empty($_POST["desc"])) {
        $descErr = "Description is required";
      } else {
        $desc = test_input($_POST["desc"]);
      }
      if (empty($_POST["mno"])) {
        $mnoErr ="Contact Number is required";
      }
      
      else
      {
        $mno = test_input($_POST["mno"]);
        if(!preg_match('/^[0-9]{10}+$/',$mno))
        {
            $mnoErr = "Only Integer Latter";
        }
      }
}
    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
      }

?>
<body style="background: linear-gradient( #ff9999, #ffcc99 ); background-repeat: no-repeat;">
    <div class="container">
    <form action="" method="POST"> 
     <h1>Authors Details :</h1>
        <table class="table" style="border:1px solid black;">
            <tr>
                <td>Name</td>
                <td><input type="text" class="form-control" name="name"  value=""> <span class="error"> <?php echo $nameErr;?></span>
  </td>
            </tr>
            <tr>
                <td>Date of Birth</td>
                <td><input type="date" name="date"></td>
            </tr>
            <tr>
                  <!-- <td>Gender</td>
                <td>Female : <input type="radio" name="gender" value="f"<?php if (isset($gender) && $gender=="f") echo "checked";?>>
                    Male :<input type="radio" name="gender" value="m" <?php if (isset($gender) && $gender=="m") echo "checked";?> >
                <span class="error"> <?php echo $genderErr;?></span>
                </td>  -->
             <td>  Gender: </td> 
             <td>
                <input type="radio"  name="gender" <?php if (isset($gender) && $gender=="f") echo "checked";?> value="female">Female            
                <input type="radio" name="gender" <?php if (isset($gender) && $gender=="m") echo "checked";?> value="male">Male
               <span class="error">* <?php echo $genderErr;?></span>
                </td>
            </tr>
            <tr>
                <td>Mobile No </td>
                <td><input type="text" name="mno" class="form-control" value=""><?php echo $mnoErr;?></td>
            </tr>
            <tr>
                <td> Description</td>
                <td><textarea name="desc" value="" class="form-control"></textarea> <span class="error"> <?php echo $descErr;?></span></td>
            </tr>
            <tr>
            <td></td>
            <td style="align:center">
            <input type="submit" value="submit" class="btn btn-warning btn-lg" name="submit" ></td>
            </tr>
        </table>
    </form>
    </div>
</body>
</html>

