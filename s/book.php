<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>

    <script>
        function add_data() {
            window.location.href = "insert_book.php";
        }
    </script>
</head>

<body>
    <div class="container">
        <?php include "header.php" ?>
        <div class="py-5">
            <button class="btn btn-outline-primary" onclick="add_data()">ADD Book</button>
        </div>
    </div>
    <?php
    include 'connection.php';

    echo "<table class='table table-hover'>";
    echo "<tr class='bg-dark text-white'>";
    echo "<th>#</th>";
    echo "<th>Title</th>";
    echo "<th>Pages</th>";
    echo "<th>Language</th>";
    echo "<th>Book Author</th>";
    echo "<th>Cover Image</th>";
    echo "<th>ISBN no</th>";
    echo "<th>Description</th>";
    echo "<th>Status</th>";
    echo "<th>Edit</th>";
    echo "<th>Delete</th>";
    echo "<th>Detail</th>";
    echo "<th>Active/Inactive</th>";
    echo "</tr>";
    $qry = "select book.id,book.title,book.pages,book.language,author.fullname,book.cover_image,book.isbn_no,book.description,book.status from book,author where book.book_author=author.id";
    $result = $conn->query($qry);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $s_id_book = $row['id'];
            echo "<tr><td>{$row['id']}</td>";
            echo "<td>{$row['title']}</td>";
            echo "<td>{$row['pages']}</td>";
            echo "<td>{$row['language']}</td>";
            echo "<td>{$row['fullname']}</td>";
            echo "<td>{$row['cover_image']}</td>";
            echo "<td>{$row['isbn_no']}</td>";
            echo "<td>{$row['description']}</td>";
            echo "<td>{$row['status']}</td>";
            echo "<td><a href='edit_book.php?s_id_book=$s_id_book' class='btn btn-dark'>Edit</a></td>";
            echo "<td><a href='delete_book.php?s_id_book=$s_id_book' class='btn btn-danger'>Delete</a></td>";
            echo "<td><a href='detail.php?s_id_book=$s_id_book' class='btn btn-secondary'>Detail</a></td>";
            if ($row['status'] == 1) {
                echo "<td><a href='active.php?s_id_book=$s_id_book' class='btn btn-danger'>InActive</td>";
            } else {
                echo "<td><a href='active.php?s_id_book=$s_id_book' class='btn btn-success'>Active</td>";
            }
        }
    }

    ?>

</body>

</html>