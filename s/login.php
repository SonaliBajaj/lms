<?php
include "login_php.php";
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script>
    $(document).ready(function() {
      $("#email").focusout(function() {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var email = $("#email").val();
        var check = re.test(email);
        if (email == "") {
          $(".email_err").html("*Required this filed");
        } else if (!check) {
          $(".email_err").html("*email not correct");
        } else {
          $(".email_err").html("");
        }
      });
      $("#pwd").focusout(function() {
        var pwd = $("#pwd").val();
        if (pwd == "") {
          $(".pwd_err").html("*Required this filed");
        } else {
          $(".pwd_err").html("");
        }
      });
    });
  </script>
  <style>
    .pwd_err,
    .email_err,
    .not {
      color: red;
    }
  </style>
</head>

<body>
  <div class="container">

    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) ?>" method="POST">
      <div class="form-group">
        <label for="exampleInputEmail1">Email address</label>
        <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email">
        <small class="email_err"><?php echo $email_err ?></small>
        <small class="form-text text-muted">We'll never share your email with anyone else.</small>

      </div>
      <div class="form-group">
        <label for="exampleInputPassword1">Password</label>
        <input type="password" name="pass" class="form-control" id="pwd" placeholder="Password">
        <small class="pwd_err"><?php echo $pwd_err ?></small>

      </div>
      <small class="not"><?php echo $not ?></small><br>
      <button type="submit" id="btn" name="btn" class="btn btn-primary">Submit</button>
    </form>
  </div>
</body>

</html>