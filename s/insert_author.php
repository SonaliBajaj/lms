<?php
include 'IU_author.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>

    <script>
        $(document).ready(function() {
            $.validator.addMethod("mobile_no", function(value, element) {
                return this.optional(element) || /^(\+\d{1,3}[- ]?)?\d{10}$/.test(value);
            }, "Please specify a valid mobile number");

            jQuery.validator.addMethod("fname", function(value, element) {
                return this.optional(element) || value == value.match(/^[a-z][a-z\s]*$/);
            }, "Only Characters Allowed and space are allowed.");
            $("#form").validate({
                rules: {
                    name: {
                        required: true,
                        fname: true
                    },
                    date: {
                        required: true,
                        date: true
                    },

                    address: {
                        required: true
                    },
                    mobile: {
                        required: true,
                        mobile_no: true
                    },
                    description: {
                        required: true
                    }
                }
            })
        });
    </script>
    <style>
        label.error {
            color: red;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="text-center card-title">Author</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <form method="post" action="" id="form">
                    <div class="form-group">
                        <label for="">Name</label>
                        <input type="text" name="name" id="name" class="form-control" placeholder="Enter Name" aria-describedby="helpId">
                        <small class="error"><?php echo $errr ?></small>
                    </div>
                    <div class="form-group">
                        <label for="">Date Of Birth</label>
                        <input type="date" name="date" id="date" class="form-control" placeholder="Enter date" aria-describedby="helpId">
                        <small class="error"><?php echo $errr ?></small>
                    </div>
                    <div class="form-group">
                        <label for="">Gender</label>
                        <div class="form-check">
                            <input type="radio" class="form-check-input" id="gender" name="gender" <?php if ($gender == "male") echo "checked"; ?> value="male" checked>
                            <label class="form-check-label" for="materialUnchecked">Male</label>
                        </div>
                        <div class="form-check">
                            <input type="radio" class="form-check-input" id="gender" name="gender" <?php if ($gender == "female") echo "checked"; ?> value="female">
                            <label class="form-check-label" for="materialUnchecked">Female</label>
                        </div>
                        <div class="form-check">
                            <input type="radio" class="form-check-input" id="gender" name="gender" <?php if ($gender == "other") echo "checked"; ?> value="other">
                            <label class="form-check-label" for="materialChecked">Other</label>
                        </div>
                        <small class="error"><?php echo $errr ?></small>
                    </div>
                    <div class="form-group">
                        <label for="">Address</label>
                        <textarea class="form-control" name="address" id="address" rows="3" id="comment"></textarea>
                        <small class="error"><?php echo $errr ?></small>
                    </div>
                    <div class="form-group">
                        <label for="">Mobile No</label>
                        <input type="text" name="mobile" id="mobile" class="form-control" placeholder="Enter Mobile No" aria-describedby="helpId">
                        <small class="error"><?php echo $errr ?></small>
                    </div>
                    <div class="form-group">
                        <label for="">Description</label>
                        <textarea class="form-control" name="description" id="description" rows="3" id="comment"></textarea>
                        <small class="error"><?php echo $errr ?></small>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="insert" id="btn" class="btn btn-success" value="Submit" aria-describedby="helpId">
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>

</html>