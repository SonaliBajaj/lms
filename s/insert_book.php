<?php
include 'IU_book.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    <style>

    </style>
    <script>
        $(document).ready(function() {
            $.validator.addMethod("isbn", function(value, element) {
                return this.optional(element) || /^(?:ISBN(?:-1[03])?:? )?(?=[0-9X]{10}$|(?=(?:[0-9]+[- ]){3})[- 0-9X]{13}$|97[89][0-9]{10}$|(?=(?:[0-9]+[- ]){4})[- 0-9]{17}$)(?:97[89][- ]?)?[0-9]{1,5}[- ]?[0-9]+[- ]?[0-9]+[- ]?[0-9X]$/.test(value);
            }, "Please specify a valid isbn number");
            $("#form").validate({
                rules: {
                    title: {
                        required: true
                    },
                    page: {
                        required: true
                    },
                    book_author: {
                        required: true
                    },
                    language: {
                        required: true
                    },

                    cover_img: {
                        required: true
                    },
                    isbn_no: {
                        required: true,
                        isbn: true
                    },
                    description: {
                        required: true
                    }
                }
            })
        });
    </script>
    <style>
        label.error {
            color: red;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="text-center card-title">Book</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) ?>" id="form">
                    <div class="form-group">
                        <label for="">Title</label>
                        <input type="text" name="title" id="title" class="form-control" placeholder="Enter Title" aria-describedby="helpId">
                        <small><?php echo $err ?></small>
                    </div>
                    <div class="form-group">
                        <label for="">Pages</label>
                        <input type="text" name="page" id="page" class="form-control" placeholder="Enter Book Page" aria-describedby="helpId">
                        <small><?php echo $err ?></small>
                    </div>
                    <div class="form-group">
                        <label for="">Language</label>
                        <input type="text" name="language" id="language" class="form-control" placeholder="Enter Book Language" aria-describedby="helpId">
                        <small><?php echo $err ?></small>
                    </div>
                    <div class="form-group">
                        <label for="">Book Author</label>
                        <select class="custom-select" name="book_author" id="book_author">
                            <option hidden value="">Book Author</option>
                            <?php
                            $qry = "select * from author";
                            $result = $conn->query($qry);
                            if ($result->num_rows > 0) {
                                while ($row = $result->fetch_assoc()) {
                                    echo "<option value='{$row['id']}'>{$row['fullname']}</option>";
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Cover image</label>
                        <input type="text" name="cover_img" id="cover_img" class="form-control" placeholder="Enter Coverpage" aria-describedby="helpId">
                        <small><?php echo $err ?></small>
                    </div>
                    <div class="form-group">
                        <label for="">ISBN NO</label>
                        <input type="text" name="isbn_no" id="isbn_no" class="form-control" placeholder="Enter ISBN NO" aria-describedby="helpId">
                        <small class="text-muted">Ex: [1-2222-3333-4]</small>
                        <small><?php echo $err ?></small>
                    </div>
                    <div class="form-group">
                        <label for="">Description</label>
                        <textarea class="form-control" id="description" name="description" rows="3"></textarea>
                        <small><?php echo $err ?></small>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="insert" id="btn" class="btn btn-success" value="Submit" aria-describedby="helpId">
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>

</html>