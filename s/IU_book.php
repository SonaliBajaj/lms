<?php
    session_start();
    include 'connection.php';
    $err="";
    function test_input($data){
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    if (isset($_POST['insert'])){
        $title = $_POST['title'];
        $page = $_POST['page'];
        $language = $_POST['language'];
        $book_author_id = $_POST['book_author'];
        $cover_img = $_POST['cover_img'];
        $isbn_no = $_POST['isbn_no'];
        $description = $_POST['description'];

        if(isset($_POST['insert'])){
            if(empty($title)){
                $errr="*required this field";
            }
            if(empty($page)){
                $errr="*required this field";
            }
            if(empty($language)){
                $errr="*required this field";
            }
            if(empty($book_author)){
                $errr="*required this field";
            }
            if(empty($cover_img)){
                $errr="*required this field";
            }
            if(empty($isbn_no)){
                $errr="*required this field";
            }else{
                if(!preg_match('/^(?:ISBN(?:-10)?:? )?(?=[0-9X]{10}$|(?=(?:[0-9]+[- ]){3})[- 0-9X]{13}$)[0-9]{1,5}[- ]?[0-9]+[- ]?[0-9]+[- ]?[0-9X]$/',test_input($isbn_no))){
                    $errr='Invalid ISBN NO';
                }
            }
            if(empty($description)){
                $errr="*required this field";
            }
        }
            $qry = "INSERT INTO book (title,pages,language,book_author,cover_image,isbn_no,description) VALUES('".$title."','".$page."','".$language."','$book_author_id','".$cover_img."','".$isbn_no."','".$description."')";
            $result = $conn->query($qry);
            if($result===TRUE){
                echo "<script>alert('data inserted successfully')</script>";
                header("location:book.php");
            }else{
                echo "{$conn->error}";
            }
    }


    //update book

    if(isset($_GET['s_id_book'])){
        $id = $_GET['s_id_book'];
        $_SESSION['s_id_book']=$id;
        $qry = "select * from book where id='$id'";
        $result = $conn->query($qry);
        if($result->num_rows>0){
            while($rows = $result->fetch_assoc()) {
                $title = $rows['title'];
                $pages = $rows['pages'];
                $language = $rows['language'];
                $book_author = $rows['book_author'];
                $cover_img = $rows['cover_image'];
                $isbn_no=$rows['isbn_no'];
                $description = $rows['description'];
            }
        }
    }
 
    if(isset($_POST['btn'])){
        $id = $_SESSION['s_id_book'];
        $title = $_POST['title'];
        $page = $_POST['page'];
        $book_language = $_POST['language'];
        $book_author = $_POST['book_author'];
        $cover_img = $_POST['cover_img'];
        $isbn_no = $_POST['isbn_no'];
        $description = $_POST['description'];

        

    $qry = "update book set title='$title',pages='$page',language='$book_language',book_author='$book_author',cover_image='$cover_img',isbn_no='$isbn_no',description='$description' where id='$id'";
    $result = $conn->query($qry);
    if($result===TRUE){
        echo "<script>alert('data successfully updated')</script>";
        header("location:book.php");
    }else{
        echo "not updated";
    }

        

    }
