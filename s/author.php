<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
        function add_data() {
            window.location.href = "insert_author.php";
        }
    </script>
</head>

<body>
    <div class="container">
        <?php include "header.php" ?>

        <div class="py-5">
            <button class="btn btn-outline-primary" onclick="add_data()">ADD Author</button>
        </div>

    </div>
    <?php
    include 'connection.php';
    $sql = "select * from author";
    $result = $conn->query($sql);
    echo "<table class='table table-hover'><tr class='bg-dark text-white'>";
    echo "<th>#</th>";
    echo "<th>NAME</th>";
    echo "<th>DOB</th>";
    echo "<th>GENDER</th>";
    echo "<th>ADDRESS</th>";
    echo "<th>MOBILE NO</th>";
    echo "<th>DESCRIPTION</th>";
    echo "<th>STATUS</th>";
    echo "<th>EDIT</th>";
    echo "<th>DELETE</th></tr>";

    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $s_id_author = $row['id'];
            echo "<tr><td>{$row['id']}</td>";
            echo "<td>{$row['fullname']}</td>";
            echo "<td>{$row['dob']}</td>";
            echo "<td>{$row['gender']}</td>";
            echo "<td>{$row['address']}</td>";
            echo "<td>{$row['mobile_no']}</td>";
            echo "<td>{$row['description']}</td>";
            echo "<td>{$row['status']}</td>";
            echo "<td><a href='edit_author.php?s_id_author=$s_id_author' class='btn btn-dark'>Edit</a></td>";
            echo "<td><a href='delete_author.php?s_id_author=$s_id_author' class='btn btn-danger'>Delete</a></td></tr>";
        }
    }
    echo "</table>";
    ?>

</body>

</html>