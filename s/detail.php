<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>

<body>
    <div class=''>
        <?php
        include 'connection.php';
        if (isset($_GET['s_id_book'])) {
            $id = $_GET['s_id_book'];
            echo "<table class='table table-hover'>";
            echo "<tr class='bg-dark text-white' ><th>#</th>";
            echo "<th>Pages</th>";
            echo "<th>Title</th>";
            echo "<th>Language</th>";
            echo "<th>Book Author</th>";
            echo "<th>Cover Image</th>";
            echo "<th>ISBN no</th>";
            echo "<th>Description</th>";
            echo "<th>Status</th></tr>";

            $qry = "select book.id,book.title,book.pages,book.language,author.fullname,book.cover_image,book.isbn_no,book.description,book.status from book inner join author on book.book_author=author.id where book.id='$id'";
            $result = $conn->query($qry);
            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    echo "<tr>";
                    $s_id_book = $row['id'];
                    echo "<td>{$row['id']}</td>";
                    echo "<td>{$row['title']}</td>";
                    echo "<td>{$row['pages']}</td>";
                    echo "<td>{$row['language']}</td>";
                    echo "<td>{$row['fullname']}</td>";
                    echo "<td>{$row['cover_image']}</td>";
                    echo "<td>{$row['isbn_no']}</td>";
                    echo "<td>{$row['description']}</td>";
                    echo "<td>{$row['status']}</td>";
                    echo "</tr>";
                }
            }
        }
        ?>
    </div>
</body>

</html>