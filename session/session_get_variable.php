<?php
session_start(); //starting php session
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <?php
         $_SESSION["firstname"] = "Sonali";
        $_SESSION["lastname"] = "Bajaj";//storing data
        echo 'Hi, ' . $_SESSION["firstname"]. '' . $_SESSION["lastname"];//Acceding data
        if(isset($_SESSION["lastname"])){
            unset($_SESSION["lastname"]);
        }
        session_destroy();
    ?>
</body>
</html>

