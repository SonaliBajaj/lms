<?php
session_start();
if(isset($_SESSION['counter']))
{
    $_SESSION['counter'] += 1;
}
else
{
    $_SESSION['counter'] = 1;
}
$msg = "you have visited this page" . $_SESSION['counter'];
$msg .= "in this session";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Session up a PHP session</title>
</head>
<body>
    <?php echo ($msg) ?>
</body>
</html>