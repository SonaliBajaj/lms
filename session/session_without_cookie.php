<?php
session_start();

if(isset($_SESSION['counter']))
{
    $_SESSION['counter'] = 1;
}
else
{
    $_SESSION['counter'] ++;
}
$msg = "you have visited this page" . $_SESSION['counter'];
$msg .= "in this session";

echo ($msg);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <p>To continue click following link<br>
    <a href="nextpage.php?<?php echo htmlspecialchars(SID);?>">
    <!-- SID = //The constant SID can also be used to retrieve the current name and session id as a string suitable for adding to URLs.  -->
</body>
</html>